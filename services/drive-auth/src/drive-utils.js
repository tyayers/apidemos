const axios = require('axios');

var config = {
  url: "https://www.googleapis.com/drive/v3/files?q=%271-W-8AVjuH4LXPZ8jDdIrl64ZAWZO830x%27%20in%20parents&corpora=allDrives&includeItemsFromAllDrives=true&supportsAllDrives=true",
  method: "get",
  headers: {
    Authorization: "Bearer " + process.env.TOKEN
  },
}

var result = "";
axios(config).then(function(response) {
  for(i=0; i<response.data.files.length; i++) {
    var file = response.data.files[i];
    if (result != "") result += ";";
    result += "claat export -auth " + process.env.TOKEN + " " + file.id;
  }
  //console.log(response.data);
}).catch(function(error) {

}).then(function() {
  console.log(result);
});