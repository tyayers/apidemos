const { google } = require('googleapis');
require('dotenv').config();

const key = require('../auth.json');
const scopes = 'https://www.googleapis.com/auth/drive.readonly';

const jwt = new google.auth.JWT(key.client_email, null, key.private_key, scopes);

jwt.authorize((err, response) => {
  console.log(response.access_token);
});